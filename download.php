<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<?php

$f = (isset($_GET['id']) ? $_GET['id'] : "no") . ".pdf";


if (!file_exists('files/'.$f)) {
    header("Location: /");
}


?>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Area FC</title> 
        <!--
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
        -->
        <link href="css/download.css" rel="stylesheet" type="text/css"/>
        
    </head>
    <body>
        <!--[if lt IE 9]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->
        


        <div class="container clearfix">


        
        <div class="content">
            <div class="wrap" id="main-content-wrap" >
                
                <p>&nbsp;</p>

                <h3>TERMS AND CONDITIONS</h3>
                <p>
                

                    Dear Talent, 
                    Our intention as a content production company is to acquire and retain the rights to use or not to use the audio, visuals and all likeness that will appear in your uploaded recording of the audition clip from the text provided. That you assign to SMATmedia all consents and all rights as is/will be contained in the contribution for use in all media, throughout the world. You also confirm that your contribution will not infringe the copyright or similar rights, of any third party. SMATmedia and all it's authorised affiliates shall have the right to use the recording of your audition video clip along with its audio in all our programming and to reproduce, distribute, transmit, retransmit and/or otherwise exploit the content via all media now known or hereafter devised. 
                    
                </p>
                
                
                <p>&nbsp;</p>
                <br/><br/>
                
                <a id="download-btn" class="btn btn-download" href="files/<?= $f ?>" target="_blank" download>I agree (start download)</a>
                
                    
                

            </div>
        
        </div>
        
            
            
        <div class="footer">

                <div id="signature">
                    &copy;2016 SMATMedia.
                </div>

        </div>
            
            
        </div>
        
        <script type="text/javascript">
            document.getElementById('download-btn').addEventListener('click', function(e) {
                // window.close();
            });
        </script>
        
    </body>
    </html>