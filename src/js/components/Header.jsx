import React from 'react';
import {connect} from 'react-redux';
import { Link } from 'react-router';
import MainNav from './MainNav';
import { SeasonMenuContainer } from './SeasonMenu';
import * as actionCreators from '../actions';

export default class Header extends React.Component {

    isActive () 
    {
        return this.props.side_nav_active;
    }

    render() {
        return (
            <div className="header">
                <Link to="/">
                <div className="logo-wrap">
                    <div className="logo">&nbsp;</div>
                </div>
                </Link>
                <button 
                type="button" 
                id="c-button--push-right" 
                onClick={() => this.props.toggleSideNav()} 
                className={"c-button c-menu__open "+(this.isActive() ? 'active' : '')}
                ><i className={"glyphicon "+(this.isActive() ? 'glyphicon-remove' : 'glyphicon-list')}></i></button>
                <MainNav {...this.props}/>
                <SeasonMenuContainer />
            </div>
                
            );

    }

}

function mapStateToProps(state) {
    return {
        main_nav_active: state.getIn(['ui', 'main_nav', 'active']), 
        side_nav_active: state.getIn(['ui', 'side_nav', 'active']), 
    };
}

export const HeaderContainer = connect(
        mapStateToProps, 
        actionCreators
        )(Header);

