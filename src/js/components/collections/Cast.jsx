import React, {Component} from 'react';
import {connect} from 'react-redux';
import * as actionCreators from '../actions';

export default class Cast extends Component {
    
    render() {
        return (
            <div className="content-body">
                <h1 className="text-primary text-center">Home</h1>
                <ul>
                {this.props.items.map(
                        (v, i) => <div><span style="background: url({v.thumb.src})"></span>{v.name}</div>
                    );
                }
            </div>
            );

    }

}

