import React from 'react';
import {connect} from 'react-redux';
import { Link } from 'react-router';
import {List, Map} from 'immutable';

export default class PhotoSlider extends React.Component {
    constructor(props) {
        super(props);
        this.state = { };
    }
    
    componentDidMount() {
        $(function() {
            $(".rslides").responsiveSlides({
                pager: true, 
                auto: true
            });
          });
    }
    
    render() {
        var items = [];
        
        for (var k in this.props.lightbox.images) {
            items.push(this.props.lightbox.images[k]);
        }
        
        return (
                <ul className="lightbox rslides">
                    { items.map(function (i, k) {

                        return  <li key={k}>
                            <div className="img" style={{backgroundImage: 'url('+i.src.large+')'}} ></div>
                                </li>
                    })}
                </ul>
        );
    }
}


