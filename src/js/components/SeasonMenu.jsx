import React from 'react';
import {connect} from 'react-redux';
import { Link } from 'react-router';
import {List, Map} from 'immutable';
import SeasonMenuGroup  from './SeasonMenuGroup';
import * as actionCreators from '../actions';


export default class SeasonMenu extends React.Component {
    constructor(props) {
        super(props);
        this.state = { active: this.props.season_nav_active };
    }
    render() {
        var active = (this.state.active || this.props.season_nav_active);
        return (
                    <div className={"season-menu"+(this.props.season_nav_active ? " active" : "")} onMouseLeave={()=>this.setState({active: false})}>
                    <div className="header">
                        <h1 className="title">Explore Episodes</h1>
                    </div>
                    <div className="content"><ul>
                        { this.props.items
                            .filter((v) => v !== undefined)
                            .map((i, k) => <li key={k}>
                                           <SeasonMenuGroup key={k} current={this.props.active_episode} season={k+1} items={i} />
                                           </li>)
                        }
                    </ul></div>
                    </div>
        );
    }
}


function mapStateToProps(state) {
    return {
        season_nav_active: state.getIn(['ui', 'season_nav', 'active']), 
        main_nav_active: state.getIn(['ui', 'main_nav', 'active']), 
        active_episode: state.get('episode'), 
        items: state.getIn(['episodes', 'list']).toJS()
    };
}

export const SeasonMenuContainer = connect(
        mapStateToProps, 
        actionCreators
        )(SeasonMenu);

