import React, {Component} from 'react';
import DocumentTitle from 'react-document-title';
import {connect} from 'react-redux';
import { Link } from 'react-router';
import {List, Map} from 'immutable';
import Spinner from '../components/Spinner';
import * as actionCreators from '../actions';
import ProfileTile from '../components/ProfileTile';
import ProfileIndex from '../components/ProfileIndex';
import { ProfileSearchBoxContainer } from '../components/ProfileSearchBox'

export default class Profiles extends Component {
    get(key) {
        return this.props.content.getIn(key);
    }
    
    getTitle() {
        return this.get(['title', 'rendered']);
    }
    
    getBody() {
        return {__html: this.get(['content', 'rendered'])};
    }
    
    
    handleCancelSearchBtnClick(e) {
        return this.props.cancelProfileSearch();
    }

    render() {
        if (this.props.ready) {
            var items = [];
            for (var k in this.props.list) items.push(this.props.list[k]);
            var pg = parseInt(this.props.page);
        }
        
        // console.log(this.props.location.query);
        //actionCreators.dispatch({type: 'LOAD_PROFILE_LIST', dispatch: //actionCreators.dispatch, location: this.props.location});
        
        return (this.props.ready) 
            ? (
            <DocumentTitle title={'Area FC: Selected Profiles'}>
                <div className="content-body m-all characters">
                    <div className="inner-wrap m-all t-4of5 d-4of5">
                        <div className="wp-header">
                        <h1 className="wp-title">First Stage Selections</h1>
                        <h3 className="page-num-badge">Page <span className="num">{pg}</span></h3>
                        <ProfileSearchBoxContainer />
                        </div>
                        { (!this.props.search_active)
                            ? 
                                <div className="wp-body m-all t-all d-all">
                                <ul className="profile-grid">
                                { items.map(function (i, k) {
                                return  <li className="m-all t-1of5 d-1of5" key={k} >
                                        <Link to={"auditions-casting/profiles/"+i.slug}           
                                              activeClassName="current_page_item">
                                            <i className="thumb" > 
                                                <img src={i.featured_image_src.thumb} />
                                            </i>
                                            <span className="label">{i.title.rendered}</span>
                                        </Link>
                                        </li>
                                })}
                                </ul>
                        
                                <div className="pagination">
                                    <button className={"prev btn " + (pg > 1 ? "btn-green" : "btn-disabled")} onClick={()=>this.props.loadProfiles(pg > 1 ? pg-1 : 1)}>Previous</button>
                                    <button className="btn btn-green next" onClick={()=>this.props.loadProfiles(items.length < 30 ? pg : pg+1)}>Next</button>
                                </div>
                                </div>
                            : 
                                <div className="wp-body m-all t-all d-all">
                                <ul className="profile-grid">
                                <li className="search-result-summary m-all t-all d-all">
                                    {
                                        this.props.search_ready 
                                        ? (this.props.search_results.length > 0 ? this.props.search_results.length : 'no' )+" profile matches"
                                        : "Searching..."
                                    }
                                <button className="close-search-btn" onClick={this.handleCancelSearchBtnClick.bind(this)}>Exit Search</button>
                                </li>
                                    
                                { this.props.search_ready 
                                    ? this.props.search_results.map(function (i, k) {
                                            return  <li className="m-all t-1of5 d-1of5" key={k} >
                                                    <Link to={"auditions-casting/profiles/"+i.slug}           
                                                          activeClassName="current_page_item">
                                                        <i className="thumb" > 
                                                            <img src={i.featured_image_src.thumb} />
                                                        </i>
                                                        <span className="label">{i.title.rendered}</span>
                                                    </Link>
                                                    </li>
                                            })
                                    : <li className="search-busy">
                                    
                                        <div className="spinner2">
                                            <div className="bounce1"></div>
                                            <div className="bounce2"></div>
                                            <div></div>
                                        </div>
                                    </li>
                                }
                                </ul>
                                </div>
                        
                        } 

                    </div>
                </div>
            </DocumentTitle>
            )
            : <Spinner />;

    }

}




function mapStateToProps(state) {
    return {  
        ready: state.getIn(['profiles', 'ready']), 
        list: state.getIn(['profiles', 'list']),
        side_nav_active: state.getIn(['ui', 'side_nav', 'active']), 
        location: state.getIn(['ui', 'location']).toJS(), 
        page: state.getIn(['profiles', 'page']), 
        
        search_active: state.getIn(['profiles', 'search', 'active']), 
        search_ready: state.getIn(['profiles', 'search', 'ready']), 
        search_results: state.getIn(['profiles', 'search', 'list']),
    };
}

export const ProfilesContainer = connect(
        mapStateToProps, 
        actionCreators
        )(Profiles);

