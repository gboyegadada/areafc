import React, {Component} from 'react';
import {connect} from 'react-redux';
import * as actionCreators from '../actions';
import {Map} from 'immutable';


export default class EpisodeBanner extends Component {
    
    render() {
        if (this.props.ready) {
            return <div id="hero-1" className="banner episode-banner" style={{backgroundImage: "url("+this.props.src.get('large')+")"}}></div>;
            
        } else {
            return <div></div>;
        }
    }

}




function mapStateToProps(state) {
    return { 
        ready: state.getIn(['episode', 'ready']), 
        src: state.getIn(['episode', 'content', 'featured_image_src']),
        side_nav_active: state.getIn(['ui', 'side_nav', 'active']), 
    };
}

export const EpisodeBannerContainer = connect(
        mapStateToProps, 
        actionCreators
        )(EpisodeBanner);

