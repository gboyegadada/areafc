var webpack = require('webpack'); // Requiring the webpack lib

module.exports = {
  entry: [
    // 'webpack-dev-server/client?http://localhost:8080', // Setting the URL for the hot reload
    // 'webpack/hot/only-dev-server', // Reload only the dev server
    './src/js/main.jsx'
  ],
  module: {
    loaders: [{
      test: /\.jsx?$/,
      exclude: /node_modules/,
      loader: 'babel' // Include the react-hot loader
    }]
  },
  resolve: {
    extensions: ['', '.js', '.jsx']
  },
  output: {
    path: __dirname + '/dist/js',
    publicPath: '/js',
    filename: 'app.js'
  },
  devServer: {
    contentBase: './dist',
    hot: false, // Activate hot loading
    historyApiFallback: {
        index: 'index.html'
    }
  },
  plugins: [
    // new webpack.HotModuleReplacementPlugin(), // Wire in the hot loading plugin
    new webpack.ProvidePlugin({
        'fetch': 'imports?this=>global!exports?global.fetch!whatwg-fetch'
    })
  ]
};
