
import React from 'react';

export default class TextInput extends React.Component {

    constructor(props) {
        super(props);
        this.state = {value: props.text};
    }

    cancelEditing() {
        this.setState({'value': this.props.text});
        return this.props.cancelEditing(this.props.id);
    }

    handleKeyDown(e) {
        switch (e.key) {
            case 'Enter':
                return this.props.doneEditing(this.props.id, this.state.value);
            case 'Escape':
                return this.cancelEditing();
        }
    }

    handleOnBlur(e) {
        return this.cancelEditing();
    }

    handleOnChange(e) {
        this.setState({'value': e.target.value});
    }

    render() {

        return <input type="text" 
                      className="edit" 
                      autoFocus={true} 
                      value={this.state.value} 
                      onChange={this.handleOnChange.bind(this)} 
                      onBlur={this.handleOnBlur.bind(this)} 
                      onKeyDown={this.handleKeyDown.bind(this)} 
        />

    }


}
