import React, {Component} from 'react';
import DocumentTitle from 'react-document-title';
import {connect} from 'react-redux';
import Spinner from '../components/Spinner';
import TeamMemberTile from '../components/TeamMemberTile';
import * as actionCreators from '../actions';

export default class Team extends Component {
    get(key) {
        return this.props.content.getIn(key);
    }
    
    getTitle() {
        return this.get(['title', 'rendered']);
    }
    
    getBody() {
        return {__html: this.get(['content', 'rendered'])};
    }
    
    
    render() {
        if (this.props.ready) {
            var items = [];
            for (var k in this.props.items) items.push(this.props.items[k]);
        }
        return (this.props.ready) 
            ? (
            <DocumentTitle title='Area FC: The Team'>
                <div className="content-body m-all team">
                    <div className="inner-wrap m-all t-4of5 d-4of5">
                        <ul>
                        { items.map((i, k) => <TeamMemberTile key={k} data={i} />)}
                        </ul>
                    </div>
                </div>
            </DocumentTitle>
                )
            : <Spinner />;
        

    }

}




function mapStateToProps(state) {
    return {  
        ready: state.getIn(['team', 'ready']), 
        items: state.getIn(['team', 'list']),
        side_nav_active: state.getIn(['ui', 'side_nav', 'active']), 
    };
}

export const TeamContainer = connect(
        mapStateToProps, 
        actionCreators
        )(Team);

