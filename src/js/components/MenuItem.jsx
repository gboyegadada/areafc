import React from 'react';
import { Link } from 'react-router';
import {List, Map} from 'immutable';
import SubMenu from './SubMenu';
import {dispatch} from '../actions';

export default class MenuItem extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = { active: false };
        
    }
    
    componentDidMount() {
        this.props.router.listen(this.handleRouteChange.bind(this));
    }
    
    componentWillUnmount() { 
        this.props.router
            .unregisterTransitionHook(this.handleRouteChange.bind(this));
    }
    
    handleRouteChange() {
        dispatch({type: "CLOSE_MENU", key: this.props.mkey});
    }
    
    
    toggleMenu(){
        // Toggle sub-menu
        //let active = !this.props.menu_active;
        
        if (this.props.menu_active) {
            dispatch({type: "CLOSE_MENU", key: null});
        } else {
            dispatch({type: "CLOSE_MENU", key: null});
            dispatch({type: "OPEN_MENU", key: this.props.mkey});
            dispatch({type: "CLOSE_SEASON_NAV"});
        }
    }
    
    isActive() {
        if (!this.props.menu) return false;
        
        return this.props.menu_active;
    }
    
    
    

    render() {
        return (
                    <li key={this.props.mkey} className="collapse">
                    {this.props.menu 
                        ? <a href="javascript:void(0);" 
                              className={this.isActive() ? "active" : ""} 
                              onClick={()=>this.toggleMenu()}>
                            <i className={"glyphicon glyphicon-"+this.props.icon}></i> {this.props.text}&nbsp;&nbsp;
                            <i className={"glyphicon glyphicon-option-"+(this.isActive() ? "vertical" : "horizontal")}></i>
                          </a> 
                        : <Link to={this.props.to} activeClassName="current_page_item">
                            <i className={"glyphicon glyphicon-"+this.props.icon}></i> {this.props.text}
                          </Link>
                    }
                    {this.isActive() ? <SubMenu items={this.props.menu} active={this.isActive()}/> : ""}
                    </li>
        );
    }
}

