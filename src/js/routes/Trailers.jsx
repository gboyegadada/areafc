import React, {Component} from 'react';
import DocumentTitle from 'react-document-title';
import {connect} from 'react-redux';
import { Link } from 'react-router';
import {List, Map} from 'immutable';
import Spinner from '../components/Spinner';
import * as actionCreators from '../actions';
import TrailerIndex from '../components/TrailerIndex';

export default class Trailers extends Component {

    getTitle() {
        return this.props.playing.title.rendered;
    }
    
    getBody() {
        return {__html: this.props.playing.content.rendered};
    }

    render() {
        if (this.props.items_ready) {
            var pg = parseInt(this.props.page);
        }
        
        var data = this.props.playing;
        
        return (this.props.ready) 
            ? (
            <DocumentTitle title='Area FC: Videos'>
                <div className="content-body m-all characters">
                    <div className="inner-wrap m-all t-4of5 d-4of5">
                        <div className="wp-header">
                        <h1 className="wp-title">Videos</h1>
                        <h3 className="page-num-badge">Page <span className="num">{pg}</span></h3>
                        </div>
                        

                        <div className="wp-body m-all t-all d-3of5" dangerouslySetInnerHTML={this.getBody(data.content.rendered)} />

                        <div className="m-all t-all d-2of5">
                        { this.props.items_ready  
                          ? <TrailerIndex items={this.props.items} />
                          : ""
                            }

                        </div>
                        <div className="pagination">
                            <button className={"prev btn " + (pg > 1 ? "btn-green" : "btn-disabled")} onClick={()=>this.props.loadTrailers(pg > 1 ? pg-1 : 1)}>Previous 10</button>
                            <button className="btn btn-green next" onClick={()=>this.props.loadTrailers(this.props.items.length < 30 ? pg : pg+1)}>Next 10</button>
                        </div>
                    </div>

                    </div>
            </DocumentTitle>
            )
            : <Spinner />;

    }

}




function mapStateToProps(state) {
    return {  
        playing: state.getIn(['trailers', 'playing', 'content']), 
        content: state.getIn(['trailers', 'playing', 'content']), 
        ready: state.getIn(['trailers', 'playing', 'ready']),
        
        items: state.getIn(['trailers', 'list']),
        items_ready: state.getIn(['trailers', 'ready']), 
        
        side_nav_active: state.getIn(['ui', 'side_nav', 'active']), 
        location: state.getIn(['ui', 'location']).toJS(), 
        page: state.getIn(['trailers', 'page']),
        
    };
}

export const TrailersContainer = connect(
        mapStateToProps, 
        actionCreators
        )(Trailers);

