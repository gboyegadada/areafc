import React, {Component} from 'react';
import {connect} from 'react-redux';
import Spinner from '../components/Spinner';
import * as actionCreators from '../actions';

export default class CastMemberTile extends Component {
    get(key) {
        return this.props.content.getIn(key);
    }
    
    getTitle() {
        return this.get(['title', 'rendered']);
    }
    
    getBody() {
        return {__html: this.get(['content', 'rendered'])};
    }
    
    
    render() {
        
        return (
            <div className="tile character">
                <div className="inner-wrap m-all t-4of5 d-4of5">
                    <h1 className="text-primary text-center">{this.getTitle()}</h1>
                </div>
            </div>
            );

    }

}


