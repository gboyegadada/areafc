import React, {Component} from 'react';
import DocumentTitle from 'react-document-title';
import {connect} from 'react-redux';
import Spinner from '../components/Spinner';
import TeamMemberIndex from '../components/TeamMemberIndex';
import {List, Map} from 'immutable';
import * as actionCreators from '../actions';

export default class TeamMember extends Component {

    get(key) {
        return this.props.content.getIn(key);
    }
    
    getTitle() {
        return this.get(['title', 'rendered']);
    }
    
    getBody(html) {
        return {__html: html};
    }
    
    
    render() {
        var 
        path = this.props.location.pathname, 
        slug = path.substring(path.lastIndexOf('/')+1), 
        
        data = this.props.items[slug];
        
        return (this.props.ready) 
            ? (
            <DocumentTitle title={'Area FC: '+data.title.rendered}>
                <div className="content-body m-all team">
                    <div className="inner-wrap m-all t-4of5 d-6of7">
                        <h1 className="wp-title">{data.title.rendered}</h1>
                        <div className="wp-body m-all t-all d-3of5" dangerouslySetInnerHTML={this.getBody(data.content.rendered)} />
                        
                        <div className="m-collapse t-collapse d-2of5">
                        <TeamMemberIndex items={this.props.items} />
                        </div>
                    </div>
                </div>
            </DocumentTitle>
                )
            : <Spinner />;
        

    }

}




function mapStateToProps(state) {
    return {  
        ready: state.getIn(['team', 'ready']), 
        items: state.getIn(['team', 'list']),
        side_nav_active: state.getIn(['ui', 'side_nav', 'active']), 
    };
}

export const TeamMemberContainer = connect(
        mapStateToProps, 
        actionCreators
        )(TeamMember);

