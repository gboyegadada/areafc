import React from 'react';
import {connect} from 'react-redux';
import { Link } from 'react-router';
import {List, Map} from 'immutable';

export default class PhotoIndex extends React.Component {
    constructor(props) {
        super(props);
        this.state = { };
    }
    render() {
        var items = [];
        
        for (var k in this.props.items) {
            items.push(this.props.items[k]);
        }
        
        return (
                    <ul className="profile-index" >
                        { items.map(function (i, k) {

                            return  <li key={k} className="">
                                    <Link to={"photos/"+i.slug}           
                                          activeClassName="current_page_item">
                                         {i.title.rendered}
                                        <i className="thumb" > 
                                            <img src={i.featured_image_src.thumb} />
                                        </i>
                                    </Link>
                                    </li>
                        })}
                    </ul>
        );
    }
}


