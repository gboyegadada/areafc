import React, {Component} from 'react';
import {connect} from 'react-redux';
import DocumentTitle from 'react-document-title';
import {BannerContainer} from '../components/Banner';
import {EpisodeContentContainer} from '../components/EpisodeContent';
import {VideoWidgetContainer} from '../components/VideoWidget';
import {TeamWidgetContainer} from '../components/TeamWidget';
import {LinksContainer} from '../components/Links';
import Spinner from '../components/Spinner';
import * as actionCreators from '../actions';
import { Link } from 'react-router';

export default class Home extends Component {
    get(key) {
        return this.props.content.getIn(key);
    }
    
    getBody() {
        return {__html: this.get(['content', 'rendered'])};
    }
    
    
    render() {
        
        return (      
            <DocumentTitle title='Area FC: Welcome'>
            <div className="content-body landing m-all">
                <div className="inner-wrap m-all t-all d-all">
                   <BannerContainer /> 
                   <div id="video-widget" className="video-widget m-all t-all d-all">
                        <h3><i className="glyphicon glyphicon-film"></i> Recent Videos</h3>
                        <VideoWidgetContainer />
                   </div>
                    
                </div>
                
            </div>
            </DocumentTitle>
            );

    }

}

