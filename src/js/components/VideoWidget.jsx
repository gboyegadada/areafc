import React from 'react';
import {connect} from 'react-redux';
import * as actionCreators from '../actions';
import { Link } from 'react-router';
import {List, Map} from 'immutable';

export default class VideoWidget extends React.Component {
    constructor(props) {
        super(props);
        this.state = { };
    }
    render() {
        var items = [];
        
        for (var k in this.props.items) {
            items.push(this.props.items[k]);
        }
        
        return (
                    <ul className="video-grid m-all t-all d-all" >
                        { this.props.items_ready ? items.map(function (i, k) {

                            return  <li key={k} className="m-all t-1of4 d-1of4">
                                    <Link to={"trailers/"+i.slug}           
                                          activeClassName="current_page_item">
                                        <i className="thumb" > 
                                            <img src={i.featured_image_src.thumb} />
                                        </i>
                                        <span className="youtube-play"><i className="glyphicon glyphicon-play"></i></span>
                                        <span className="video-title">{i.title.rendered}</span>
                                    </Link>
                                    </li>
                        }).slice(0,4) : "" }
                    </ul>
        );
    }
}





function mapStateToProps(state) {
    return {  
        playing: state.getIn(['trailers', 'playing', 'content']), 
        content: state.getIn(['trailers', 'playing', 'content']), 
        ready: state.getIn(['trailers', 'playing', 'ready']),
        
        items: state.getIn(['trailers', 'list']),
        items_ready: state.getIn(['trailers', 'ready'])
    };
}

export const VideoWidgetContainer = connect(
        mapStateToProps, 
        actionCreators
        )(VideoWidget);

