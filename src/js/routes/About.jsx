import React from 'react';
import {connect} from 'react-redux';
import DocumentTitle from 'react-document-title';
import * as actionCreators from '../actions';

export default class About extends React.Component {

    get(key) {
        return this.props.content.getIn(key);
    }
    render() {
        
        return (
            <DocumentTitle title='Area FC: About the Show'>
            <div className="content-body">
                <h1 className="text-primary text-center">{this.get(['title', 'rendered'])}</h1>
            </div>
            </DocumentTitle>
            );

    }

}

