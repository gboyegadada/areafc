import React, {Component} from 'react';
import {connect} from 'react-redux';
import * as actionCreators from '../actions';

import Home from '../routes/Home';
import About from '../routes/About';
import Contact from '../routes/Contact';
import Auditions from '../routes/Auditions';
import WhereToWatch from '../routes/WhereToWatch';
import Trailers from '../routes/Trailers';
import NoMatch from '../routes/NoMatch';

import Spinner from '../components/Spinner';


export default class Page extends Component {
    
    componentWillUnmount() {
        // this.serverRequest.abort();
    }


    render() {
        
        if (this.props.ready && this.props.episode_list_ready) {
            
            switch (this.props.route.path) {
                case '/': return <Home {...this.props} />;
                case '/home': return <Home {...this.props} />;
                case '/about': return <About {...this.props} />;
                case '/contact': return <Contact {...this.props} />;
                case '/auditions-casting': return <Auditions {...this.props} />;
                case '/where-to-watch': return <WhereToWatch {...this.props} />;
                case '/trailers': return <Trailers {...this.props} />;
                default: return <NoMatch />;
            }
            
        } else {
            return <Spinner />;
        }
    }

}




function mapStateToProps(state) {
    return {
        content: state.getIn(['page', 'content']), 
        featured_img_src: state.getIn(['page', 'featured_image_src']), 
        episode_list_ready: state.getIn('episode_list', 'ready'), 
        ready: state.getIn(['page', 'ready']), 
        side_nav_active: state.getIn(['ui', 'side_nav', 'active']), 
    };
}

export const PageContainer = connect(
        mapStateToProps, 
        actionCreators
        )(Page);

