import React, {Component} from 'react';
import DocumentTitle from 'react-document-title';
import {connect} from 'react-redux';
import { Link } from 'react-router';
import Spinner from '../components/Spinner';
import * as actionCreators from '../actions';
import ProfileIndex from '../components/ProfileIndex';

export default class Profile extends Component {
    get(key) {
        return this.props.content.getIn(key);
    }

    getTitle() {
        return this.get(['title', 'rendered']);
    }

    getBody(html) {
        return {__html: html};
    }


    render() {
        var 
        path = this.props.location.pathname, 
        slug = path.substring(path.lastIndexOf('/')+1), 

        data = this.props.content;

        return (this.props.ready) 
            ? (
            <DocumentTitle title={'Area FC: '+data.title.rendered}>
                <div className="content-body m-all team">
                    <div className="inner-wrap m-all t-4of5 d-6of7">

                        <div className="pagination">
                            <Link to={"auditions-casting/profiles"} className="btn btn-green prev">Back</Link>
                        </div>

                        <h1 className="wp-title">{data.title.rendered}</h1>
                        <div className="wp-body m-all t-all d-3of5" dangerouslySetInnerHTML={this.getBody(data.content.rendered)} />

                        <div className="m-collapse t-collapse d-2of5">
                        { this.props.items_ready  
                          ? <ProfileIndex items={this.props.items} />
                          : ""
                            }

                        </div>
                    </div>
                </div>
            </DocumentTitle>
                )
            : <Spinner />;

    }

}




function mapStateToProps(state) {
    return {  
        ready: state.getIn(['profile', 'ready']), 
        content: state.getIn(['profile', 'content']),
        items: state.getIn(['profiles', 'list']),
        items_ready: state.getIn(['profiles', 'ready']),
        side_nav_active: state.getIn(['ui', 'side_nav', 'active']), 
    };
}

export const ProfileContainer = connect(
        mapStateToProps, 
        actionCreators
        )(Profile);

