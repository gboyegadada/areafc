import React from 'react';
import {connect} from 'react-redux';
import { Link } from 'react-router';
import {List, Map} from 'immutable';

export default class SocialNav extends React.Component {
    render() {
        return (
                    <ul>
                        { this.props.items.map(function (i, k) {
                            return  <li key={k}><a href={i.to} target="_blank">
                                    <i className={"socicon-"+i.icon}></i> {i.text}
                                    </a></li>
                        })}
                    </ul>
        );
    }
}



function mapStateToProps(state) {
    return {
         items: state.getIn(['ui', 'social_nav', 'items']).toJS()
    };
}

export const SocialNavContainer = connect(
        mapStateToProps
        )(SocialNav);