<?php

require_once 'app/App.php';
require_once 'vendor/autoload.php';
require_once 'wp/wp_head.php';

\App::boot();

use App\Controller;

$slim = new \Slim\App;

// - /home
$slim->get('/home', new Controller\Home);


$slim->run();
