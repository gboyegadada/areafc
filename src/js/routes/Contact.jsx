import React from 'react';
import DocumentTitle from 'react-document-title';
import { RouteHandler } from 'react-router';

export default class Contact extends React.Component {

    get(key) {
        return this.props.content.getIn(key);
    }
    
    getBody() {
        return {__html: this.get(['content', 'rendered'])};
    }
    render() {
        
        return (
            <DocumentTitle title='Area FC: Contact Us'>
            <div className="content-body m-all t-all d-6of7">
                <div className="inner-wrap">
                <h1 className="wp-title text-primary text-center">{this.get(['title', 'rendered'])}</h1>
                <div className="wp-body" dangerouslySetInnerHTML={this.getBody()} />
                </div>
                
            </div>
            </DocumentTitle>
            );

    }

}