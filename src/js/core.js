import {List, Map, fromJS} from 'immutable';

export const INITIAL_STATE = fromJS({
        ui: {
            
            location: {
                
            }, 
            
            main_nav: {
                active: false
            }, 
            
            side_nav: {
                active: false
            },  
            
            season_nav: {
                active: false
            }, 
            
            /*
            | 2. Auditions and casting *
            ------------------------------------------------------------
            | 4. The show(AreaFC TV Series, AreaFC Radio, AreaFC Comic)
            | 3. Trailers
            | 5. Characters
            | 6. Cast
            ------------------------------------------------------------
            | 7. The Team(Show creator, Story team), *
            | 8. Crew
            | 9. Where to Watch *
            | 10. Contact us *
            */
            page_nav: {
                title: '',
                items: [
                    // {text: 'Home', icon: 'th-large', to: '/'}, 
                    {text: 'The Show', icon: 'info-sign', to: 'the-show', 
                     menu_active: false, 
                     menu: [
                        {text: 'Videos', icon: 'film', to: 'trailers'}, 
                        {text: 'Photos', icon: 'th', to: 'photos'}, 
                        {text: 'Meet the Characters', icon: 'user', to: 'characters'},
                        {text: 'Meet the Cast', icon: 'star', to: 'cast'}
                     ]
                    }, 
                    {text: 'The Team', icon: 'fire', to: 'team', 
                     menu_active: false, 
                     menu: []
                    }, 
                    {text: 'Auditions', icon: 'bullhorn', to: 'auditions-casting', 
                     menu_active: false, 
                     menu: [
                        {text: 'Selected Profiles', icon: 'user', to: 'auditions-casting/profiles'}
                     ]
                    },
                    {text: 'Watch', icon: 'film', to: 'where-to-watch'}, 
                    {text: 'Contact', icon: 'map-marker', to: 'contact'}
                ]
            },  
            social_nav: {
                title: 'Social',
                items: [
                    {text: 'Facebook', icon: 'facebook', to: 'https://facebook.com/'}, 
                    {text: 'Twitter', icon: 'twitter', to: 'https://twitter.com/areafctv'}, 
                    {text: 'Youtube', icon: 'youtube', to: 'https://youtube.com'}, 
                    {text: 'Instagram', icon: 'instagram', to: 'https://instagram.com/'}
                ]
            }, 
            active_episode: {
                episode: 1,
                season: 6
            }
        }, 
        episodes: {
            ready: false,
            list: []
        }, 
        episode: {
            ready: false, 
            id: null,
            index: 0, 
            season: 0, 
            content: {}
        }, 
        cast: {
            ready: false,
            list: []
        }, 
        characters: {
            ready: false,
            list: [],
            page: 1
        }, 
        character: {
            ready: false, 
            id: null,
            content: {}
        }, 
        profiles: { // Auditioned actors
            ready: false, 
            list: [],
            page: 1,
            search: { // Search results
                active: false, 
                ready: false, 
                list: []
            }
        }, 
        profile: {
            ready: false, 
            id: null,
            content: {}
        }, 
        trailers: {
            ready: false, 
            list: [],
            page: 1,
            playing: {
                ready: false, 
                content: {}
            }
        }, 
        photos: {
            ready: false, 
            list: [],
            page: 1,
            lightbox: {
                ready: false, 
                content: {}
            }
        }, 
        team: {
            ready: false,
            list: []
        }, 
        page: {
            uri: {
                pathname: '/',
                hash: '',
            },
            ready: false,
            content: {}
        }
    });


