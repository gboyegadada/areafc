import React, {Component} from 'react';
import {connect} from 'react-redux';
import * as actionCreators from '../actions';
import {Map} from 'immutable';


export default class ProfileSearchBox extends Component {

    constructor(props) {
        super(props);
        this.state = {value: props.text};
    }

    cancelEditing() {
        this.setState({'value': this.props.text});
        return this.props.cancelProfileSearch();
    }

    handleKeyDown(e) {
        switch (e.key) {
            case 'Enter':
                return this.props.doProfileSearch(this.state.value);
            case 'Escape':
                return this.cancelEditing();
        }
    }

    handleClick(e) {
        return this.props.doProfileSearch(this.state.value);
    }

    handleCancelBtnClick(e) {
        return this.cancelEditing();
    }

    handleOnFocus(e) {
        // return this.props.startProfileSearch();
    }

    handleOnBlur(e) {
        // return this.cancelEditing();
    }

    handleOnChange(e) {
        this.setState({'value': e.target.value});
    }

    render() {

        return <div className="search-widget m-all t-2of5 d-2of5">
            <input type="text" 
                      className="search-box" 
                      placeholder="..."  
                      autoFocus={false} 
                      value={this.state.value} 
                      onFocus={this.handleOnFocus.bind(this)} 
                      onChange={this.handleOnChange.bind(this)} 
                      onBlur={this.handleOnBlur.bind(this)} 
                      onKeyDown={this.handleKeyDown.bind(this)} 
            />
            <button className="search-btn" onClick={this.handleClick.bind(this)}>search</button>
            </div>

    }
    
}




function mapStateToProps(state) {
    return {  
        search_active: state.getIn(['profiles', 'search', 'active']), 
        side_nav_active: state.getIn(['ui', 'side_nav', 'active']), 
    };
}

export const ProfileSearchBoxContainer = connect(
        mapStateToProps, 
        actionCreators
        )(ProfileSearchBox);

