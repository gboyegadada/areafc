import React from 'react';
import {connect} from 'react-redux';
import { Link } from 'react-router';
import {List, Map} from 'immutable';

export default class SeasonMenuGroup extends React.Component {
    getBody(html) {
        return {__html: html};
    }
    
    isActive(s, ep) {
        let c = this.props.current.toJS();
        
        return c.season == s && c.index == ep;
    }
    
    render() {
        return (
                    <ul className={"season-menu-tile"} >
                        <li className="title-card">
                            <div className={"header bg-color-s"+this.props.season}>
                            <div className={"meta txt-color-s"+this.props.season}>Season</div>
                            <h3 className="title">{this.props.season}</h3>
                            </div>
                        </li>
                        { this.props.items
                            .filter((v) => v !== undefined)
                            .map((i, k) => {
                                let cl = i.featured_image_src !== undefined ? "thumb" : "";
                                return  <li key={k} className={"episode-menu-tile collapse bg-color-s"+i.meta.season} id={'ep' + i.meta.season + '' + i.meta.index}>
                                        <Link to={"season/"+i.meta.season+"/episode/"+i.meta.index} activeClassName="current_page_item">
                                            <div className={"header bg-color-s"+i.meta.season}>
                                            <div className={"meta txt-color-s"+i.meta.season}>
                                                {"Season "+i.meta.season+" Episode "+i.meta.index}
                                            </div>
                                            <h3 className="title">{i.title.rendered}</h3>
                                            </div>
                                            <div className="thumb">
                                                <img src={i.featured_image_src.large} />
                                            </div>
                                            <div className="excerpt" dangerouslySetInnerHTML={this.getBody(i.excerpt.rendered)}></div>
                                            <div className="dash">
                                            {this.isActive(i.meta.season, i.meta.index)
                                                ? <button className={"ep-btn current"+(this.isActive(i.meta.season, i.meta.index)  ? ' txt-color-s'+i.meta.season : '')}>Current Episode</button>
                                                : <button className="ep-btn">Select Episode</button>
                                            }
                                            </div>
                                        </Link>
                                        </li>
                        })}
                    </ul>
        );
    }
}

