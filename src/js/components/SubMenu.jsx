import React from 'react';
import {connect} from 'react-redux';
import { Link } from 'react-router';
import {List, Map} from 'immutable';

export default class SubMenu extends React.Component {
    constructor(props) {
        super(props);
        this.state = { active: this.props.active };
    }
    render() {
        var active = (this.state.active || this.props.active);
        return (
                    <ul className={"sub-menu"+(active ? " active" : "")} onMouseLeave={()=>this.setState({active: false})}>
                        { this.props.items.map(function (i, k) {
                            let cl = "";
                            cl = i.icon !== undefined ? "glyphicon glyphicon-"+i.icon+" " : "";
                            cl += i.thumb !== undefined ? "thumb" : "";
                    
                            return  <li key={k} className="collapse">
                                    <Link to={i.to} activeClassName="current_page_item">
                                        <i 
                                            className={cl} >
                                            {i.thumb !== undefined ? <img src={i.thumb} /> : ""}
                                        </i> {i.text}&nbsp;&nbsp;
                                    </Link>
                                    </li>
                        })}
                    </ul>
        );
    }
}

