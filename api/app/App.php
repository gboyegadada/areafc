<?php

class App
{

    /**
     * @var static
     */
    public static $instance;
    
    /**
     *
     * @var Container
     */
    protected $container;


    private function __construct() {
        
        $this->container = [];
    }

    /**
     * 
     * @return \App
     */
    public static function boot()
    {
        if (empty(self::$instance)) {
            
            // Initialize...
            self::$instance = new self ();
            self::registerAutoload();
        }
        
        return self::$instance;
    }
    
    public static function shutdown()
    {
    }
    
    public function __invoke() {
        return self::boot();
    }
    
    /**
     * 
     * @return Container
     */
    public function getContainer() 
    {
        return $this->container;
    }

    /**
     * Internal autoloader for spl_autoload_register().
     *
     * @param string $class
     */
    public static function autoload($class)
    {
        // namesapace \App
        if (0 === strpos($class, 'App')) {
        
            $path = __DIR__.'/'.str_replace('\\', DIRECTORY_SEPARATOR, $class).'.php';

            if (!file_exists($path)) { 
                return; 
            } 
            
        } 
    }
        

    /**
     * Configure autoloading .
     *
     * This is designed to play nicely with other autoloaders.
     *
     * @param mixed $callable A valid PHP callable that will be called when autoloading the first FPP class
     */
    public static function registerAutoload($prepend = false)
    {
        if (PHP_VERSION_ID < 50300) {
            spl_autoload_register(array(__CLASS__, 'autoload'));
        } else {
            spl_autoload_register(array(__CLASS__, 'autoload'), true, $prepend);
        }
        
    }
    
    
    
}
