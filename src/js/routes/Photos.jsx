import React, {Component} from 'react';
import DocumentTitle from 'react-document-title';
import {connect} from 'react-redux';
import { Link } from 'react-router';
import {List, Map} from 'immutable';
import Spinner from '../components/Spinner';
import * as actionCreators from '../actions';
import PhotoSlider from '../components/PhotoSlider';
import PhotoIndex from '../components/PhotoIndex';

export default class Photos extends Component {

    getTitle() {
        return this.props.lightbox.title.rendered;
    }
    
    getBody() {
        return {__html: this.props.lightbox.content.rendered};
    }

    render() {
        if (this.props.items_ready) {
            var pg = parseInt(this.props.page);
        }
        
        var data = this.props.lightbox;
        
        return (this.props.ready) 
            ? (
            <DocumentTitle title='Area FC: Photos'>
                <div className="content-body m-all characters">
                    <div className="inner-wrap m-all t-4of5 d-4of5">
                        <div className="wp-header">
                        <h1 className="wp-title">Photos: {this.getTitle()}</h1>
                        {/*
                        <h3 className="page-num-badge">Page <span className="num">{pg}</span></h3>
                        */}
                        </div>
                        

                        <div className="wp-body m-all t-all d-3of5">
                            <PhotoSlider lightbox={this.props.lightbox} />
                        </div>

                        <div className="m-all t-all d-2of5">
                        { this.props.items_ready  
                          ? <PhotoIndex items={this.props.items} />
                          : ""
                            }

                        </div>
                        <div className="pagination">
                            <button className={"prev btn " + (pg > 1 ? "btn-green" : "btn-disabled")} onClick={()=>this.props.loadPhotos(pg > 1 ? pg-1 : 1)}>Previous 10</button>
                            <button className="btn btn-green next" onClick={()=>this.props.loadPhotos(this.props.items.length < 30 ? pg : pg+1)}>Next 10</button>
                        </div>
                    </div>

                    </div>
            </DocumentTitle>
            )
            : <Spinner />;

    }

}




function mapStateToProps(state) {
    return {  
        lightbox: state.getIn(['photos', 'lightbox', 'content']), 
        content: state.getIn(['photos', 'lightbox', 'content']), 
        ready: state.getIn(['photos', 'lightbox', 'ready']),
        
        items: state.getIn(['photos', 'list']),
        items_ready: state.getIn(['photos', 'ready']), 
        
        side_nav_active: state.getIn(['ui', 'side_nav', 'active']), 
        location: state.getIn(['ui', 'location']).toJS(), 
        page: state.getIn(['photos', 'page']),
        
    };
}

export const PhotosContainer = connect(
        mapStateToProps, 
        actionCreators
        )(Photos);

