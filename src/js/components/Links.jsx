import React from 'react';
import {connect} from 'react-redux';
import * as actionCreators from '../actions';
import { Link } from 'react-router';
import {List, Map} from 'immutable';

export default class Links extends React.Component {
    constructor(props) {
        super(props);
        this.state = { };
    }
    render() {
        var items = [];
        
        for (var k in this.props.items) {
            items.unshift(this.props.items[k]);
        }
        
        return (
            <div id="panels" className="m-all t-all d-all">
                <div className="panel-header">
                    <h3>Links:</h3>
                </div>
                <div className="panels-inner-wrap m-all t-4of5 d-4of5">
                    <div className="panel resource m-all t-1of4 d-1of4">
                        <i className="glyphicon glyphicon-grain"></i>
                        <br/>
                        <p><a href="http://www.niyiakinmolayan.com" target="_blank">Resource<br/><small>www.niyiakinmolayan.com</small></a></p>
                    </div>
                    <div className="panel nollywood m-all t-1of4 d-1of4">
                        <i className="glyphicon glyphicon-bullhorn"></i>
                        <br/>
                        <p><a href="http://www.tns.ng" target="_blank">Nollywood<br/><small>www.tns.ng</small></a></p>
                    </div>
                    <div className="panel twitter m-all t-1of4 d-1of4">
                        <i className="glyphicon glyphicon-th-list"></i>
                        <br/>
                        <p><a href="https://twitter.com/areafctv" target="_blank">Twitter<br/><small>@AreaFCTV</small></a></p>
                    </div>
                    <div className="panel smatmedia m-all t-1of4 d-1of4">
                        <i className="glyphicon glyphicon-facetime-video"></i>
                        <br/>
                        <p><a href="http://www.smatmedia.com" target="_blank">SMATMedia<br/><small>www.smatmedia.com</small></a></p>
                    </div>
                    <br className="cf"/>
                </div>
                <div className="panel-footer">
                    <div className="copyright">&copy;2016 SMATMedia</div>
                </div>
            </div>
        );
    }
}





function mapStateToProps(state) {
    return {  
        playing: state.getIn(['trailers', 'playing', 'content']), 
        content: state.getIn(['trailers', 'playing', 'content']), 
        ready: state.getIn(['trailers', 'playing', 'ready']),
        
        items: state.getIn(['trailers', 'list']),
        items_ready: state.getIn(['trailers', 'ready'])
    };
}

export const LinksContainer = connect(
        mapStateToProps, 
        actionCreators
        )(Links);

