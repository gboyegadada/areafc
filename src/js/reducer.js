import {Map, fromJS} from 'immutable';
import {INITIAL_STATE} from './core';

function setState(state, newState) {
    return state.merge(newState);
}


function openSideNav(menuState) {
    return menuState.set('active', true);
}

function closeSideNav(menuState) {
    return menuState.set('active', false);
}

function toggleSideNav(menuState) {
    return menuState.set('active', !menuState.get('active'));
}

function openMainNav(menuState) {
    return menuState.set('active', true);
}

function closeMainNav(menuState) {
    return menuState.set('active', false);
}

function toggleMainNav(menuState) {
    return menuState.set('active', !menuState.get('active'));
}

function openMenu(menuState, key) {
    if (key === null || key === undefined) {
        let items = menuState.get('items');
        return menuState.set('items', items.map((item)=>item.set('menu_active', true)));
    }
    
    return menuState.updateIn(['items', key, 'menu_active'], menu_active => true);
}

function closeMenu(menuState, key) {
    if (key === null || key === undefined) {
        let items = menuState.get('items');
        return menuState.set('items', items.map((item)=>item.set('menu_active', false)));
    }
    return menuState.updateIn(['items', key, 'menu_active'], menu_active => false);
}

function toggleMenu(menuState, key) {
    if (key === null || key === undefined) return menuSate;
        
    return menuState.updateIn(['items', key, 'menu_active'], menu_active => !menu_active);
}

function toggleSeasonNav(menuState) {
    return menuState.set('active', !menuState.get('active'));
}

function openSeasonNav(menuState) {
    return menuState.set('active', true);
}

function closeSeasonNav(menuState) {
    return menuState.set('active', false);
}


function setLocation(locState, location) {
    return locState.merge(location);
}

function loadTeamList(listState, dispatch) {
    fetch('/wp/wp-json/wp/v2/team/?order=asc&order_by=id')
        .then(response => response.json())
        .then((data)=>{
            let menu = data.map((i, k)=>{
                return {text: i.title.rendered, thumb: i.featured_image_src.thumb, to: "/team/"+i.slug};
            });
            
            dispatch({type: 'SET_TEAM_LIST', data });
            dispatch({type: 'UPDATE_TEAM_MENU', menu });
        })
        .catch((e)=>{
        
            throw new TypeError(e.message);

        });
    return listState.set('ready', false);
}

function setTeamList(listState, data) {
    let list = {};
    var item;
    
    for (var i=0,l=data.length; i < l; i++) {
        list[data[i].slug] = data[i];
    }
    
    return listState.set('list', list).set('ready', true);
}

function updateTeamMenu(menuState, menu) {
    return menuState.set('menu', menu);
}


function loadCharacterList(listState, dispatch) {
    fetch('/wp/wp-json/wp/v2/characters/?order=asc&order_by=id')
        .then(response => response.json())
        .then((data)=>{
            let menu = data.map((i, k)=>{
                return {text: i.title.rendered, thumb: i.featured_image_src.thumb, to: "/characters/"+i.slug};
            });
            
            dispatch({type: 'SET_CHARACTER_LIST', data });
            // dispatch({type: 'UPDATE_CHARACTER_MENU', menu });
        })
        .catch((e)=>{
        
            throw new TypeError(e.message);

        });
    return listState.set('ready', false);
}

function setCharacterList(listState, data) {
    let list = {};
    var item;
    
    for (var i=0,l=data.length; i < l; i++) {
        list[data[i].slug] = data[i];
    }
    
    return listState.set('list', list).set('ready', true);
}

function loadProfileList(listState, dispatch, page) {
    var 
    pg = (page > 0) ? page : listState.get('page'),
    q = '&page='+pg;
    
    if (listState.get('page') == page || (page == 0 && listState.get('ready') === true)) {
        return listState.set('ready', true);
    } else {
        fetch('/wp/wp-json/wp/v2/audition-profiles/?order=asc&orderby=title&per_page=30'+q)
            .then(response => response.json())
            .then((data)=>{
                let menu = data.map((i, k)=>{
                    return {text: i.title.rendered, thumb: i.featured_image_src.thumb, to: "/profiles/"+i.slug};
                });

                dispatch({type: 'SET_PROFILE_LIST', data });
            })
            .catch((e)=>{

                throw new TypeError(e.message);

            });
    }
    
    return listState.set('ready', false).set('page', pg);
}

function setProfileList(listState, data) {
    let list = {};
    var item;
    
    for (var i=0,l=data.length; i < l; i++) {
        list[data[i].slug] = data[i];
    }
    
    return listState.set('list', list).set('ready', true);
}



function loadProfile(listState, dispatch, location) {
    var 
    slug = location.pathname.substring(location.pathname.lastIndexOf('/')+1);
    
    fetch('/wp/wp-json/wp/v2/audition-profiles?filter[name]='+slug)
        .then(response => response.json())
        .then((data)=>{
            dispatch({type: 'SET_PROFILE', data: data[0] });
        })
        .catch((e)=>{
        
            throw new TypeError(e.message);

        });
    return listState.set('ready', false);
}

function setProfile(listState, data) {
    return listState.set('content', data).set('ready', true);
}


function doProfileSearch(state, term, dispatch) {
    var q = '&filter[s]='+encodeURIComponent(term);
    
    fetch('/wp/wp-json/wp/v2/audition-profiles/?order=asc&order_by=id&per_page=20'+q)
        .then(response => response.json())
        .then((data)=>{
            let menu = data.map((i, k)=>{
                return {text: i.title.rendered, thumb: i.featured_image_src.thumb, to: "/profiles/"+i.slug};
            });
            
            dispatch({type: 'SET_PROFILE_SEARCH_RESULTS', data });
        })
        .catch((e)=>{
        
            throw new TypeError(e.message);

        });
    return state.set('ready', false).set('active', true);
}

function setProfileSearchResults(state, data) {
    let list = {};
    
    return state.set('list', data).set('ready', true);
}

function cancelProfileSearch(state) {
    return state.set('list', []).set('ready', false).set('active', false);
}

function startProfileSearch(state) {
    return state.set('list', []).set('active', true);
}



/* --------------------- TRAILERS ----------------------- */


function loadTrailerList(listState, dispatch, page) {
    var 
    pg = (page > 0) ? page : listState.get('page'),
    q = '&page='+pg;
    
    if (listState.get('page') == page || (page == 0 && listState.get('ready') === true)) {
        return listState.set('ready', true);
    } else {
        fetch('/wp/wp-json/wp/v2/trailers?order=desc&orderby=id&per_page=10'+q)
            .then(response => response.json())
            .then((data)=>{
                let menu = data.map((i, k)=>{
                    return {text: i.title.rendered, thumb: i.featured_image_src.thumb, to: "/trailers/"+i.slug};
                });
                dispatch({type: 'SET_TRAILER_LIST', data });
                dispatch({type: 'SET_TRAILER', data: data[0] });
            });
    }
    
    return listState.set('ready', false).set('page', pg);
}

function setTrailerList(listState, data) {
    let list = {};
    var item;
    
    for (var i=0,l=data.length; i < l; i++) {
        list[data[i].slug] = data[i];
    }
    
    return listState.set('list', list).set('ready', true);
}



function loadTrailer(listState, dispatch, location) {
    var 
    slug = location.pathname.substring(location.pathname.lastIndexOf('/')+1);
    
    fetch('/wp/wp-json/wp/v2/trailers?filter[name]='+slug)
        .then(response => response.json())
        .then((data)=>{
            dispatch({type: 'SET_TRAILER', data: data[0] });
        })
        .catch((e)=>{
        
            throw new TypeError(e.message);

        });
    return listState.set('ready', false);
}

function setTrailer(listState, data) {
    return listState.set('content', data).set('ready', true);
}




/* --------------------- PHOTOS ----------------------- */


function loadGalleries(listState, dispatch, page) {
    var 
    pg = (page > 0) ? page : listState.get('page'),
    q = '&page='+pg;
    
    if (listState.get('page') == page || (page == 0 && listState.get('ready') === true)) {
        return listState.set('ready', true);
    } else {
        fetch('/wp/wp-json/wp/v2/galleries?order=desc&orderby=id&per_page=10'+q)
            .then(response => response.json())
            .then((data)=>{
                let menu = data.map((i, k)=>{
                    return {text: i.title.rendered, thumb: i.featured_image_src.thumb, to: "/galleries/"+i.slug};
                });
                dispatch({type: 'SET_GALLERIES', data });
                dispatch({type: 'SET_GALLERY', data: data[0] });
            });
    }
    
    return listState.set('ready', false).set('page', pg);
}

function setGalleries(listState, data) {
    let list = {};
    var item;
    
    for (var i=0,l=data.length; i < l; i++) {
        list[data[i].slug] = data[i];
    }
    
    return listState.set('list', list).set('ready', true);
}

function loadGallery(listState, dispatch, location) {
    var 
    slug = location.pathname.substring(location.pathname.lastIndexOf('/')+1);
    
    fetch('/wp/wp-json/wp/v2/galleries?filter[name]='+slug)
        .then(response => response.json())
        .then((data)=>{
            dispatch({type: 'SET_GALLERY', data: data[0] });
        })
        .catch((e)=>{
        
            throw new TypeError(e.message);

        });
    return listState.set('ready', false);
}

function setGallery(listState, data) {
    return listState.set('content', data).set('ready', true);
}






function loadCastList(listState, dispatch) {
    fetch('/wp/wp-json/wp/v2/cast/?order=asc&order_by=id')
        .then(response => response.json())
        .then((data)=>{
            let menu = data.map((i, k)=>{
                return {text: i.title.rendered, thumb: i.featured_image_src.thumb, to: "/cast/"+i.slug};
            });
            
            dispatch({type: 'SET_CAST_LIST', data });
        })
        .catch((e)=>{
        
            throw new TypeError(e.message);

        });
    return listState.set('ready', false);
}

function setCastList(listState, data) {
    let list = {};
    var item;
    
    while (item = data.pop()) {
        list[item.slug] = item;
    }
    
    return listState.set('list', list).set('ready', true);
}


function loadEpisodeList(listState, dispatch) {
    fetch('/wp/wp-json/wp/v2/episodes/?order=desc&order_by=id')
        .then(response => response.json())
        .then((data)=>{
            let episode = data[0].meta;
            dispatch({type: 'SET_EPISODE_LIST', data });
            dispatch({type: 'SET_EPISODE', episode });
        })
        .catch((e)=>{
        
            throw new TypeError(e.message);

        });
    return listState.set('ready', false);
}

function setEpisodeList(listState, data) {
    let episodes = [];
    var ep;
    
    while (ep = data.pop()) {
        var i = parseInt(ep.meta.index), s = parseInt(ep.meta.season);
        if (episodes[s] === undefined) episodes[s] = [];
        episodes[s][i] = ep;
    }
    
    return listState.set('list', fromJS(episodes)).set('ready', true);
}

function setEpisode(state, ep, dispatch) {
    let ready = state.getIn(['episodes', 'ready']);
    
    if (ready) {
        let data = state.getIn(['episodes', 'list', ep.season, ep.index]);
        if (!data) throw new TypeError('Unknown episode: [ep'+ep.index + ', s'+ep.season+']');

        let epState = {
            ready: true,
            id: data.get('id'), 
            index: data.getIn(['meta', 'index']), 
            season: data.getIn(['meta', 'season']), 
            content: data
        };

        return state.updateIn(['episode'], episode => episode.merge(epState));
    }
    
    // try again in 1000ms
    else setTimeout(()=>{ dispatch({type: 'SET_EPISODE', episode: ep, dispatch}); }, 1000);
    
    return state;
}

function loadPageContent(pageState, loc, dispatch) {

    return pageState.set('ready', false);
}

function setPageContent(pageState, newState, dispatch) {
    //"http://dev.areafc.tv/wp/wp-json/wp/v2/media/20"
    
    return pageState
        .set('ready', true)
        .updateIn(['content'], content => content.merge(newState));
}



export default (state = INITIAL_STATE, action) => {
    switch(action.type) {
        case 'SET_STATE':
            return setState(state, action.state);
        case 'OPEN_SIDE_NAV':
            return state.updateIn(['ui','side_nav'], menuState => openSideNav(menuState));
        case 'CLOSE_SIDE_NAV':
            return state.updateIn(['ui','side_nav'], menuState => closeSideNav(menuState));
        case 'TOGGLE_SIDE_NAV':
            return state.updateIn(['ui','side_nav'], menuState => toggleSideNav(menuState));
            
        case 'OPEN_MAIN_NAV':
            return state.updateIn(['ui','main_nav'], menuState => openMainNav(menuState));
        case 'CLOSE_MAIN_NAV':
            return state.updateIn(['ui','main_nav'], menuState => closeMainNav(menuState));
        case 'TOGGLE_MAIN_NAV':
            return state.updateIn(['ui','main_nav'], menuState => toggleMainNav(menuState));
            
        case 'OPEN_MENU':
            return state.updateIn(['ui','page_nav'], menuState => openMenu(menuState, action.key));
        case 'CLOSE_MENU':
            return state.updateIn(['ui','page_nav'], menuState => closeMenu(menuState, action.key));
        case 'TOGGLE_MENU':
            return state.updateIn(['ui','page_nav'], menuState => toggleMenu(menuState, action.key));
            
        case 'TOGGLE_SEASON_NAV':
            return state.updateIn(['ui','season_nav'], menuState => toggleSeasonNav(menuState));
            
        case 'OPEN_SEASON_NAV':
            return state.updateIn(['ui','season_nav'], menuState => openSeasonNav(menuState));
            
        case 'CLOSE_SEASON_NAV':
            return state.updateIn(['ui','season_nav'], menuState => closeSeasonNav(menuState));
            
        case 'SET_LOCATION':
            return state.updateIn(['ui','location'], locState => setLocation(locState, action.location));
            
        case 'SET_EPISODE':
            return setEpisode(state, action.episode, action.dispatch);
        case 'SET_EPISODE_FEATURED_IMAGE':
            return state.updateIn(['episode', 'featured_image'], imgState => setFeaturedImage(imgState, action.data));
        case 'LOAD_EPISODE_LIST':
            return state.updateIn(['episodes'], listState => loadEpisodeList(listState, action.dispatch));
        case 'SET_EPISODE_LIST':
            return state.updateIn(['episodes'], listState => setEpisodeList(listState, action.data));
            
        case 'LOAD_TEAM_LIST':
            return state.updateIn(['team'], listState => loadTeamList(listState, action.dispatch));
        case 'SET_TEAM_LIST':
            return state.updateIn(['team'], listState => setTeamList(listState, action.data));
            
        case 'LOAD_CHARACTER_LIST':
            return state.updateIn(['characters'], listState => loadCharacterList(listState, action.dispatch));
        case 'SET_CHARACTER_LIST':
            return state.updateIn(['characters'], listState => setCharacterList(listState, action.data));
            
        case 'LOAD_PROFILE_LIST':
            return state.updateIn(['profiles'], listState => loadProfileList(listState, action.dispatch, action.page, action.location));
        case 'SET_PROFILE_LIST':
            return state.updateIn(['profiles'], listState => setProfileList(listState, action.data));
        case 'LOAD_PROFILE':
            return state.updateIn(['profile'], listState => loadProfile(listState, action.dispatch, action.location));
        case 'SET_PROFILE':
            return state.updateIn(['profile'], listState => setProfile(listState, action.data));
            
        case 'PROFILE_SEARCH':
            return state.updateIn(['profiles', 'search'], searchState => doProfileSearch(searchState, action.term, action.dispatch));
        case 'SET_PROFILE_SEARCH_RESULTS':
            return state.updateIn(['profiles', 'search'], searchState => setProfileSearchResults(searchState, action.data));
        case 'START_PROFILE_SEARCH':
            return state.updateIn(['profiles', 'search'], searchState => startProfileSearch(searchState));
        case 'CANCEL_PROFILE_SEARCH':
            return state.updateIn(['profiles', 'search'], searchState => cancelProfileSearch(searchState));
            
        case 'LOAD_TRAILER_LIST':
            return state.updateIn(['trailers'], listState => loadTrailerList(listState, action.dispatch, action.page, action.location));
        case 'SET_TRAILER_LIST':
            return state.updateIn(['trailers'], listState => setTrailerList(listState, action.data));
        case 'LOAD_TRAILER':
            return state.updateIn(['trailers', 'playing'], listState => loadTrailer(listState, action.dispatch, action.location));
        case 'SET_TRAILER':
            return state.updateIn(['trailers', 'playing'], listState => setTrailer(listState, action.data));
            
        case 'LOAD_GALLERIES':
            return state.updateIn(['photos'], listState => loadGalleries(listState, action.dispatch, action.page, action.location));
        case 'SET_GALLERIES':
            return state.updateIn(['photos'], listState => setGalleries(listState, action.data));
        case 'LOAD_GALLERY':
            return state.updateIn(['photos', 'lightbox'], listState => loadGallery(listState, action.dispatch, action.location));
        case 'SET_GALLERY':
            return state.updateIn(['photos', 'lightbox'], listState => setGallery(listState, action.data));
            
            
        case 'LOAD_CAST_LIST':
            return state.updateIn(['cast'], listState => loadCastList(listState, action.dispatch));
        case 'SET_CAST_LIST':
            return state.updateIn(['cast'], listState => setCastList(listState, action.data));
            
            
        case 'LOAD_PAGE_CONTENT':
            return state.updateIn(['page'], pageState => loadPageContent(
                pageState, 
                action.location, 
                action.dispatch
            ));
        case 'SET_PAGE_STATE':
            return state.updateIn(['page'], pageState => setPageContent(pageState, action.state, action.dispatch));
            
        case 'UPDATE_TEAM_MENU':
            return state.updateIn(['ui', 'page_nav', 'items', '1'], menuState => updateTeamMenu(menuState, action.menu));
    }
    
    return state;
}
