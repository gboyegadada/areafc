import React from 'react';
import { Router, Route, browserHistory } from 'react-router';
import { dispatch, closeSideNav } from './actions';
import {AppContainer} from './containers/App';
import {PageContainer} from './containers/Page';

import Home from './routes/Home';
import About from './routes/About';
import Contact from './routes/Contact';
import Auditions from './routes/Auditions';
import {PhotosContainer} from './routes/Photos';
import {TrailersContainer} from './routes/Trailers';
import NoMatch from './routes/NoMatch';
import {EpisodeContainer} from './routes/Episode';
import {TeamContainer} from './routes/Team';
import {TeamMemberContainer} from './routes/TeamMember';
import {CharactersContainer} from './routes/Characters';
import {CharacterContainer} from './routes/Character';
import {CastContainer} from './routes/Cast';
import {CastMemberContainer} from './routes/CastMember';
import {ProfilesContainer} from './routes/Profiles';
import {ProfileContainer} from './routes/Profile';

const locationChanged = (e) => {
    
    dispatch({type: 'CLOSE_SIDE_NAV'});
    dispatch({type: 'CLOSE_MAIN_NAV'});
    dispatch({type: 'SET_LOCATION', location: e.location});
    
    const epRegx = /^\/?season\/(\d+)\/episode\/(\d+)/;
    
    const teamRegx = /^\/?team\/?/;
    const teamMemRegx = /^\/?team\/(.*)/;
    
    const castRegx = /^\/?cast\/?/;
    const castMemRegx = /^\/?cast\/(\d+)/;
    
    const charactersRegx = /^\/?characters\/?/;
    const characterRegx = /^\/?characters\/(\d+)/;
    
    const profilesRegx = /^\/?auditions-casting\/profiles\/?$/;
    const profileRegx = /^\/?auditions-casting\/profiles\/([a-zA-Z0-9\-]+)/;
    
    const trailersRegx = /^\/?trailers\/?$/;
    const trailerRegx = /^\/?trailers\/([a-zA-Z0-9\-]+)/;
    
    const galleriesRegx = /^\/?photos\/?$/;
    const galleryRegx = /^\/?photos\/([a-zA-Z0-9\-]+)/;
    
    var m;
    let loc = e.location;
    
    // ? Episode
    if (m = loc.pathname.match(epRegx)) {
        // Set episode
        dispatch({type: 'SET_EPISODE', episode: {index: m[2], season: m[1]}, dispatch});
        
        // Hide menu
        dispatch({type: 'CLOSE_SEASON_NAV'});
    }
    
    // ? Team
    else if (m = loc.pathname.match(teamRegx)) {
        // Set episode
        //dispatch({type: 'SET_EPISODE', episode: {index: m[1], season: m[2]}, dispatch});
    } 
    
    // ? Team / Member
    else if (m = loc.pathname.match(teamMemRegx)) {
        // Set episode
        // dispatch({type: 'SET_EPISODE', episode: {index: m[1], season: m[2]}, dispatch});
    } 
    
    // ? Cast
    else if (m = loc.pathname.match(castRegx)) {
        // Set episode
        //dispatch({type: 'SET_EPISODE', episode: {index: m[1], season: m[2]}, dispatch});
    } 
    
    // ? Cast Member
    else if (m = loc.pathname.match(castMemRegx)) {
        // Set episode
        //dispatch({type: 'SET_EPISODE', episode: {index: m[1], season: m[2]}, dispatch});
    } 
    
    // ? Characters
    else if (m = loc.pathname.match(charactersRegx)) {
        // Set episode
        //dispatch({type: 'SET_EPISODE', episode: {index: m[1], season: m[2]}, dispatch});
    } 
    
    // ? Character
    else if (m = loc.pathname.match(characterRegx)) {
        // Set episode
        //dispatch({type: 'SET_EPISODE', episode: {index: m[1], season: m[2]}, dispatch});
    } 
    
    // ? Profiles
    else if (m = loc.pathname.match(profilesRegx)) {
        // Set episode
        //dispatch({type: 'SET_EPISODE', episode: {index: m[1], season: m[2]}, dispatch});
        dispatch({type: 'LOAD_PROFILE_LIST', dispatch: dispatch, page: 0, location: e.location});
    } 
    
    
    // ? Profile
    else if (m = loc.pathname.match(profileRegx)) {
        // Set episode
        //dispatch({type: 'SET_EPISODE', episode: {index: m[1], season: m[2]}, dispatch});
        dispatch({type: 'LOAD_PROFILE', dispatch: dispatch, location: e.location});
    } 
    
    // ? Trailers
    else if (m = loc.pathname.match(trailersRegx)) {
        dispatch({type: 'LOAD_TRAILER_LIST', dispatch: dispatch, page: 0});
    } 
    
    // ? Trailer
    else if (m = loc.pathname.match(trailerRegx)) {
        dispatch({type: 'LOAD_TRAILER', dispatch: dispatch, location: e.location});
    } 
    
    // ? Galleries
    else if (m = loc.pathname.match(galleriesRegx)) {
        dispatch({type: 'LOAD_GALLERIES', dispatch: dispatch, page: 0});
    } 
    
    // ? Gallery
    else if (m = loc.pathname.match(galleryRegx)) {
        dispatch({type: 'LOAD_GALLERY', dispatch: dispatch, location: e.location});
    } 
    
    // : Page
    else {
        dispatch({type: 'LOAD_PAGE_CONTENT', location: loc, dispatch});
        
        // Fetch page content here...
        let path = loc.pathname == '/' ? 'home' : loc.pathname.replace(/^[\/]/, '');
        fetch('/wp/wp-json/wp/v2/pages/?slug='+path)
            .then(response => response.json())
            .then((data) => {
                dispatch({type: 'SET_PAGE_STATE', state: data[0], dispatch});
            });
    }
};

export const routes = (
  <Router history={browserHistory}>
    <Route component={AppContainer} >
      <Route path="/" component={PageContainer} onEnter={locationChanged} />
      <Route path="/home" component={PageContainer} onEnter={locationChanged} />
      <Route path="/about" component={PageContainer} onEnter={locationChanged} />
      <Route path="/the-show" component={PageContainer} onEnter={locationChanged} />
      <Route path="/contact" component={PageContainer} onEnter={locationChanged} />
        
      <Route path="/auditions-casting" component={PageContainer} onEnter={locationChanged} />
      <Route path="/auditions-casting/profiles/*" component={ProfileContainer} onEnter={locationChanged} />
      <Route path="/auditions-casting/profiles" component={ProfilesContainer} onEnter={locationChanged} />
        
      <Route path="/where-to-watch" component={PageContainer} onEnter={locationChanged} />
      <Route path="/trailers" component={TrailersContainer} onEnter={locationChanged} />
      <Route path="/trailers/*" component={TrailersContainer} onEnter={locationChanged} />
      <Route path="/photos" component={PhotosContainer} onEnter={locationChanged} />
      <Route path="/photos/*" component={PhotosContainer} onEnter={locationChanged} />
      <Route path="/season/*" component={EpisodeContainer} onEnter={locationChanged} />
      <Route path="/team" component={TeamContainer} onEnter={locationChanged} />
      <Route path="/team/*" component={TeamMemberContainer} onEnter={locationChanged} />
      <Route path="/characters" component={CharactersContainer} onEnter={locationChanged} />
      <Route path="/characters/*" component={CharacterContainer} onEnter={locationChanged} />
      <Route path="/cast" component={CastContainer} onEnter={locationChanged} />
      <Route path="/cast/*" component={CastMemberContainer} onEnter={locationChanged} />
      <Route path="*" component={PageContainer} onEnter={locationChanged} />
    </Route>
  </Router>
  );

  

