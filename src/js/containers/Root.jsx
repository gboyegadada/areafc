import React, {Component} from 'react';
import {Provider} from 'react-redux';
import {routes} from '../routes';
import makeStore from '../store';

export const store = makeStore();

store.dispatch({type: 'LOAD_EPISODE_LIST', dispatch: store.dispatch});
store.dispatch({type: 'LOAD_TEAM_LIST', dispatch: store.dispatch});
store.dispatch({type: 'LOAD_CHARACTER_LIST', dispatch: store.dispatch});
store.dispatch({type: 'LOAD_CAST_LIST', dispatch: store.dispatch});
store.dispatch({type: 'LOAD_TRAILER_LIST', dispatch: store.dispatch, page: 0});
store.dispatch({type: 'LOAD_GALLERIES', dispatch: store.dispatch, page: 0});

export default class Root extends Component {

    render() {
        return (
                <Provider store={store}>
                    {routes}
                </Provider>
               );
    }

}

