import React from 'react';
import {connect} from 'react-redux';
import { Link } from 'react-router';
import {List, Map} from 'immutable';
import {SeasonMenuBtnContainer} from './SeasonMenuBtn';
import SubMenu from './SubMenu';
import MenuItem from './MenuItem';

export default class PageNav extends React.Component {

    
    render() {
        return (
                    <ul>
                        <SeasonMenuBtnContainer active={false} />
                        <li className="home-btn"><Link to="/" activeClassName="current_page_item">
                        <i className="glyphicon glyphicon-th-large"></i> Home
                        </Link></li>
                        { this.props.items.map(
                            (i, k) => <MenuItem key={k} 
                                                mkey={k} 
                                                active={false} {...i} 
                                                router={this.context.router} 
                                                location={this.props.location}
                                                main_nav_active={this.props.main_nav_active} side_nav_active={this.props.side_nav_active}/>
                        )}
                    </ul>
        );
    }
}


PageNav.contextTypes = {
    router: React.PropTypes.object.isRequired
};



function mapStateToProps(state) {
    return {
         location: state.getIn(['ui', 'location']).toJS(), 
         main_nav_active: state.getIn(['ui', 'main_nav', 'active']), 
         side_nav_active: state.getIn(['ui', 'side_nav', 'active']), 
         items: state.getIn(['ui', 'page_nav', 'items']).toJS(), 
         active_episode: {
             index: state.getIn(['episode', 'index']), 
             season: state.getIn(['episode', 'season']) 
         }
    };
}

export const PageNavContainer = connect(
        mapStateToProps
        )(PageNav);