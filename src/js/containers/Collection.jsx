import React, {Component} from 'react';
import {connect} from 'react-redux';
import * as actionCreators from '../actions';

import Cast from '../components/collections/Cast.jsx';
import Character from '../components/collections/Character.jsx';
import Seasons from '../components/collections/Seasons.jsx';
import Episodes from '../components/collections/Episodes.jsx';

import Spinner from '../components/Spinner';


export default class Collection extends Component {
    
    componentWillUnmount() {
        // this.serverRequest.abort();
    }


    render() {
        
        if (this.props.ready) {
            
            switch (this.props.type) {
                case 'cast': return <Cast {...this.props} />;
                case 'characters': return <Characters {...this.props} />;
                case 'seasons': return <Seasons {...this.props} />;
                case 'edisodes': return <Episodes {...this.props} />;
                default: throw new TypeError('Unknow collection type "'+this.props.type+'"');
            }
            
        } else {
            return <Spinner />;
        }
    }

}




function mapStateToProps(state) {
    return {
        content: state.getIn(['page', 'content']), 
        ready: state.getIn(['page', 'ready']), 
        side_nav_active: state.getIn(['ui', 'side_nav', 'active']), 
    };
}

export const CollectionContainer = connect(
        mapStateToProps, 
        actionCreators
        )(Collection);

