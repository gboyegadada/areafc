import React, {Component} from 'react';
import {connect} from 'react-redux';
import * as actionCreators from '../actions';
import {Map} from 'immutable';


export default class Banner extends Component {

    render() {
        if (this.props.ready) {
            return <div id="hero-1" className="banner"><img src={this.props.src.retina} /></div>;
            
        } else {
            return <div></div>;
        }
    }

}




function mapStateToProps(state) {
    return {  
        ready: state.getIn(['page', 'ready']), 
        src: state.getIn(['page', 'content', 'featured_image_src']).toJS(),
        side_nav_active: state.getIn(['ui', 'side_nav', 'active']), 
    };
}

export const BannerContainer = connect(
        mapStateToProps, 
        actionCreators
        )(Banner);

