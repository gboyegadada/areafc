import {store} from './containers/Root';

export function dispatch(action) {
    store.dispatch(action);

    return action;
}

export function openSideNav() {
    return {
        type: 'OPEN_SIDE_NAV'
    };
}
    
export function closeSideNav() {
    return {
        type: 'CLOSE_SIDE_NAV'
    };
}
    
export function toggleSideNav() {
    return {
        type: 'TOGGLE_SIDE_NAV'
    };
}


export function openMainNav() {
    return {
        type: 'OPEN_MAIN_NAV'
    };
}
    
export function closeMainNav() {
    return {
        type: 'CLOSE_MAIN_NAV'
    };
}
    
export function toggleMainNav() {
    return {
        type: 'TOGGLE_MAIN_NAV'
    };
}
    
export function toggleSeasonNav() {
    return {
        type: 'TOGGLE_SEASON_NAV'
    };
}
    
export function openSeasonNav() {
    return {
        type: 'OPEN_SEASON_NAV'
    };
}
    
export function closeSeasonNav() {
    return {
        type: 'CLOSE_SEASON_NAV'
    };
}
    
export function setState(new_state) {
    return {
        type: 'SET_STATE', 
        state: new_state
    };
}
       
export function setEpisode(episode) {
    return {
        type: 'SET_EPISODE', 
        episode, 
        dispatch
    };
}

export function loadPageContent(e) {
    return {
        type: 'LOAD_PAGE_CONTENT', e
    };
}

export function loadProfiles(page) {
    return {
        type: 'LOAD_PROFILE_LIST', 
        page, 
        dispatch
    };
}

export function doProfileSearch(term) {
    return {
        type: 'PROFILE_SEARCH', 
        term, 
        dispatch
    };
}

export function cancelProfileSearch() {
    return {
        type: 'CANCEL_PROFILE_SEARCH'
    };
}

export function startProfileSearch() {
    return {
        type: 'START_PROFILE_SEARCH'
    };
}
     


export function loadTrailers(page) {
    return {
        type: 'LOAD_TRAILER_LIST', 
        page, 
        dispatch
    };
}

export function loadGalleries(page) {
    return {
        type: 'LOAD_GALLERIES', 
        page, 
        dispatch
    };
}