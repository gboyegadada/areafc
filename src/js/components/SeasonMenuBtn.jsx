import React from 'react';
import {connect} from 'react-redux';
import { Link } from 'react-router';
import * as actionCreators from '../actions';
import {dispatch} from '../actions';
import SeasonMenu from './SeasonMenu';

export default class SeasonMenuBtn extends React.Component {
    constructor(props) {
        super(props);
        this.state = { active: this.props.active };
    }
    
    getActive(key) {
        return  this.props.active_episode.get(key);
    }
    
    toggleMenu(){
        // Toggle sub-menu
        //let active = !this.state.active;
        
        //this.setState({active});
        dispatch({type: "TOGGLE_SEASON_NAV"});
        dispatch({type: "CLOSE_MENU", key: null});
    }
    
    isActive() {
        return this.props.season_nav_active;
    }
    
    
    
    
    render() {
        return (
                <li className={"season-menu-btn"+(!this.isActive()  ? ' bg-color-s'+this.getActive('season') : '')+(this.isActive() ? ' active' : '')}>
                <button 
                type="button" 
                id="c-button--push-right" 
                onClick={() => this.toggleMenu()} 
                className={" "+(this.isActive() ? 'active' : '')}
                >
                    <i className="glyphicon glyphicon-th-large"></i>&nbsp;&nbsp;
                    {this.isActive() 
                        ? "Change Episode"
                        : <span><strong>S{this.getActive('season')}</strong>&nbsp;
                        EP<span className="collapse">ISODE</span> {this.getActive('index')}</span>
                    }
                    &nbsp;&nbsp;<i className={"glyphicon glyphicon-option-"+(this.isActive() ? "vertical" : "horizontal")}></i>
                </button>
                </li>
            );

    }

}

function mapStateToProps(state) {
    return {
        season_nav_active: state.getIn(['ui', 'season_nav', 'active']), 
        main_nav_active: state.getIn(['ui', 'main_nav', 'active']), 
        active_episode: state.get('episode'), 
        items: state.getIn(['episodes', 'list']).toJS()
    };
}

export const SeasonMenuBtnContainer = connect(
        mapStateToProps, 
        actionCreators
        )(SeasonMenuBtn);

