import React from 'react';
import {connect} from 'react-redux';
import { Link } from 'react-router';
import {List, Map} from 'immutable';

export default class ProfileIndex extends React.Component {
    constructor(props) {
        super(props);
        this.state = { };
    }
    render() {
        // var active = (this.state.active || this.props.active);
        var items = [];
        
        for (var k in this.props.items) {
            items.unshift(this.props.items[k]);
        }
        
        return (
                    <div className="index-wrap">
                    <ul className="profile-index" >
                        { items.map(function (i, k) {

                            return  <li key={k} className="">
                                    <Link to={"auditions-casting/profiles/"+i.slug}           
                                          activeClassName="current_page_item">
                                         {i.title.rendered}
                                        <i className="thumb" > 
                                            <img src={i.featured_image_src.thumb} />
                                        </i>
                                    </Link>
                                    </li>
                        })}
                    </ul>
                    </div>
        );
    }
}


