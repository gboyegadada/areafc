import React from 'react';
import {connect} from 'react-redux';
import * as actionCreators from '../actions';
import { Link } from 'react-router';
import {List, Map} from 'immutable';

export default class TeamWidget extends React.Component {
    constructor(props) {
        super(props);
        this.state = { };
    }
    render() {
        var items = [];
        
        for (var k in this.props.items) {
            items.unshift(this.props.items[k]);
        }
        
        return (
                    <ul className="profile-index" >
                        { this.props.items_ready ? items.map(function (i, k) {

                            return  <li key={k} className="">
                                    <Link to={"trailers/"+i.slug}           
                                          activeClassName="current_page_item">
                                         {i.title.rendered}
                                        <i className="thumb" > 
                                            <img src={i.featured_image_src.thumb} />
                                        </i>
                                    </Link>
                                    </li>
                        }) : "" }
                    </ul>
        );
    }
}





function mapStateToProps(state) {
    return {  
        items: state.getIn(['team', 'list']),
        items_ready: state.getIn(['team', 'ready'])
    };
}

export const TeamWidgetContainer = connect(
        mapStateToProps, 
        actionCreators
        )(TeamWidget);

