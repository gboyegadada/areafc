import React from 'react';
import { Link } from 'react-router';
import MainNav from './MainNav';
import {LinksContainer} from './Links';

export default class Footer extends React.Component {

    render() {
        return (
            <div className="footer">
            
                <LinksContainer />
            </div>
                
            );

    }

}