import React from 'react';
import {HeaderContainer} from '../components/Header';
import Footer from '../components/Footer';
import {SideNavContainer} from '../components/SideNav';
import {connect} from 'react-redux';
import * as actionCreators from '../actions';

export default class App extends React.Component {

    render() {
        return (
            <div className={"site-container "+(this.props.side_nav_active ? "side-nav-active" : "")} id="site-container">
                <SideNavContainer />
                <HeaderContainer location={this.props.location} />
                {this.props.children}
                <div className="clearfix"></div>
                <Footer />
            </div>
            );

    }

}

function mapStateToProps(state) {
    return {
        page: {
            ready: state.getIn(['page', 'ready'])
        },
        side_nav_active: state.getIn(['ui', 'side_nav', 'active']), 
    };
}

export const AppContainer = connect(
        mapStateToProps, 
        actionCreators
        )(App);
