import React, {Component} from 'react';
import DocumentTitle from 'react-document-title';
import {connect} from 'react-redux';
import {BannerContainer} from '../components/Banner';
import Spinner from '../components/Spinner';
import * as actionCreators from '../actions';
import { Link } from 'react-router';

export default class Auditions extends Component {
    get(key) {
        return this.props.content.getIn(key);
    }
    
    getBody() {
        return {__html: this.get(['content', 'rendered'])};
    }
    
    
    render() {
        
        return (
            <DocumentTitle title='Area FC: Auditions'>
            <div className="content-body m-all">
                <BannerContainer />
                <div className="inner-wrap m-all t-4of5 d-4of5">
                    <h1 className="wp-title">{this.get(['title', 'rendered'])}</h1>
                    <div className="wp-body m-all t-all d-3of5" dangerouslySetInnerHTML={this.getBody(this.get(['content', 'rendered']))} />
                </div>
            </div>
            </DocumentTitle>
            );

    }

}

