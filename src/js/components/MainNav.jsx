import React from 'react';
import { Link } from 'react-router';
import {PageNavContainer} from './PageNav';

export default class MainNav extends React.Component {
    isActive () 
    {
        return this.props.side_nav_active;
    }
    render() {
        return (
                <nav id="main-nav" className={"nav t-2of3 "+(this.isActive() ? 'collapse' : '')}>
                    <PageNavContainer {...this.props} />
                </nav>
        );
    }
}