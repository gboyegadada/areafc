import React, {Component} from 'react';
import DocumentTitle from 'react-document-title';
import {connect} from 'react-redux';
import {EpisodeBannerContainer} from '../components/EpisodeBanner';
import {EpisodeContentContainer} from '../components/EpisodeContent';
import Spinner from '../components/Spinner';
import * as actionCreators from '../actions';

export default class Episode extends Component {
    get(key) {
        return this.props.content.getIn(key);
    }
    
    getTitle() {
        return this.get(['title', 'rendered']);
    }
    
    getBody() {
        return {__html: this.get(['content', 'rendered'])};
    }
    
    
    render() {
        
        return (
            <DocumentTitle title={'Area FC: Episode - '+this.getTitle()}>
            <div className="content-body m-all">
                <EpisodeBannerContainer />
                <div className="inner-wrap m-all t-4of5 d-4of5">
                    <h1 className="text-primary text-center">{this.getTitle()}</h1>
                    <EpisodeContentContainer />
                </div>
            </div>
            </DocumentTitle>
            );

    }

}




function mapStateToProps(state) {
    return {  
        ready: state.getIn(['episode', 'ready']), 
        content: state.getIn(['episode', 'content']),
        side_nav_active: state.getIn(['ui', 'side_nav', 'active']), 
    };
}

export const EpisodeContainer = connect(
        mapStateToProps, 
        actionCreators
        )(Episode);

