import React from 'react';
import {connect} from 'react-redux';
import { Link } from 'react-router';
import * as actionCreators from '../actions';
import {PageNavContainer} from './PageNav';
import {SocialNavContainer} from './SocialNav';

export default class SideNav extends React.Component {
    getActiveEpisode() {
        return  "Season "+this.props.active_episode.season+
                " Episode "+this.props.active_episode.index;
    }
    render() {
        return (
            <div 
            className={"side-nav-container "+(this.props.side_nav_active ? "is_active" : "")} 
            id="side-nav-container">
                <nav id="side-nav" className="side-nav">
                    <h3 className="nav-header">{this.getActiveEpisode()}</h3>
                    <PageNavContainer />
                    <h3 className="nav-header">Social</h3>
                    <SocialNavContainer />
                </nav>
            </div>
        );
    }
}



function mapStateToProps(state) {
    return {
         side_nav_active: state.getIn(['ui', 'side_nav', 'active']), 
         active_episode: {
             index: state.getIn(['episode', 'index']), 
             season: state.getIn(['episode', 'season']) 
         }
    };
}

export const SideNavContainer = connect(
        mapStateToProps, 
        actionCreators
        )(SideNav);

