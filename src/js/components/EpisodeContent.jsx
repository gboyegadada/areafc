import React, {Component} from 'react';
import {connect} from 'react-redux';
import Spinner from './Spinner';
import * as actionCreators from '../actions';


export default class EpisodeContent extends Component {
    get(key) {
        return this.props.content.getIn(key);
    }
    
    getBody() {
        return {__html: this.get(['content', 'rendered'])};
    }
    
    
    render() {
        if (this.props.ready) {
            return <div className="wp-body" dangerouslySetInnerHTML={this.getBody()} />;
        } else {
            return <Spinner />;
        }
    }

}




function mapStateToProps(state) {
    return {  
        ready: state.getIn(['episode', 'ready']), 
        content: state.getIn(['episode', 'content']),
        side_nav_active: state.getIn(['ui', 'side_nav', 'active']), 
    };
}

export const EpisodeContentContainer = connect(
        mapStateToProps, 
        actionCreators
        )(EpisodeContent);

