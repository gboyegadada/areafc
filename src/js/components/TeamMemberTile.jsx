import React, {Component} from 'react';
import {connect} from 'react-redux';
import Spinner from '../components/Spinner';
import * as actionCreators from '../actions';
import { Link } from 'react-router';

export default class TeamMemberTile extends Component {
    get(key) {
        return this.props.data[key];
    }
    
    getTitle() {
        return this.props.data.title.rendered;
    }
    
    getBody() {
        return {__html: this.get(['content', 'rendered'])};
    }
    
    
    render() {
        
        return (
            <li className="tile team member">
                <div className="inner-wrap m-all t-4of5 d-4of5">
                    <h4 className="text-primary text-center">
                        <Link to={"/team/"+this.props.data.slug} activeClassName="current_page_item">
                        {this.props.data.title.rendered}
                        </Link>
                    </h4>
                </div>
            </li>
            );

    }

}


