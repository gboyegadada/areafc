import React, {Component} from 'react';
import DocumentTitle from 'react-document-title';
import {connect} from 'react-redux';
import { Link } from 'react-router';
import {List, Map} from 'immutable';
import Spinner from '../components/Spinner';
import * as actionCreators from '../actions';

export default class Cast extends Component {
    get(key) {
        return this.props.content.getIn(key);
    }
    
    getTitle() {
        return this.get(['title', 'rendered']);
    }
    
    getBody() {
        return {__html: this.get(['content', 'rendered'])};
    }
    
    
    render() {
        if (this.props.ready) {
            var items = [];
            for (var k in this.props.list) items.push(this.props.list[k]);
        }
        return (this.props.ready) 
            ? (
            <DocumentTitle title='Area FC: Meet the Cast'>
                <div className="content-body m-all characters">
                    <div className="inner-wrap m-all t-4of5 d-4of5">
                        <h1 className="wp-title">Meet The Cast</h1>
                        <div className="wp-body m-all t-all d-3of5">
                        <ul className="profile-grid">
                        { items.map(function (i, k) {
                            return  <li className="m-all t-2of6 d-1of5" key={k} >
                                    <Link to={"cast/"+i.slug}           
                                          activeClassName="current_page_item">
                                        <i className="thumb" > 
                                            <img src={i.featured_image_src.thumb} />
                                        </i>
                                        <span className="label">{i.title.rendered}</span>
                                    </Link>
                                    </li>
                        })} 
                        </ul>
                        </div>

                    </div>
                </div>
            </DocumentTitle>
            )
            : <Spinner />;

    }

}




function mapStateToProps(state) {
    return {  
        ready: state.getIn(['cast', 'ready']), 
        list: state.getIn(['cast', 'list']),
        side_nav_active: state.getIn(['ui', 'side_nav', 'active']), 
    };
}

export const CastContainer = connect(
        mapStateToProps, 
        actionCreators
        )(Cast);

