import React, {Component} from 'react';

export default class Spinner extends Component {

    render() {
        return (
            <div className="content-body">
                <div className="areafc-spinner"></div>
            </div>
            );

    }

}

