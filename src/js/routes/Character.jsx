import React, {Component} from 'react';
import DocumentTitle from 'react-document-title';
import {connect} from 'react-redux';
import { Link } from 'react-router';
import Spinner from '../components/Spinner';
import * as actionCreators from '../actions';
import CharacterIndex from '../components/CharacterIndex';

export default class Character extends Component {
    get(key) {
        return this.props.content.getIn(key);
    }
    
    getTitle() {
        return this.get(['title', 'rendered']);
    }
    
    getBody(html) {
        return {__html: html};
    }
    
    
    render() {
        var 
        path = this.props.location.pathname, 
        slug = path.substring(path.lastIndexOf('/')+1), 
        
        data = this.props.items[slug];
        
        return (this.props.ready) 
            ? (
            <DocumentTitle title={'Area FC: '+data.title.rendered}>
                <div className="content-body m-all team">
                    <div className="inner-wrap m-all t-4of5 d-6of7">
                        <div className="wp-header">
                        <div className="pagination">
                            <Link to={"characters"} className="btn btn-green prev">Back</Link>
                        </div>
                        <h1 className="wp-title">{data.title.rendered}</h1>
                        </div>

                        
                        
                        <div className="wp-body m-all t-all d-3of5" dangerouslySetInnerHTML={this.getBody(data.content.rendered)} />
                        
                        <div className="m-collapse t-collapse d-2of5 pull-right">
                        <CharacterIndex items={this.props.items} />
                        </div>
                        
                        <div className="m-all t-all d-3of5">
                        <a className="btn btn-green" href={"/download.php?id="+slug} target="_blank">Download Audition Text</a>
                        </div>
                        
                    </div>
                </div>
            </DocumentTitle>
                )
            : <Spinner />;

    }

}




function mapStateToProps(state) {
    return {  
        ready: state.getIn(['characters', 'ready']), 
        items: state.getIn(['characters', 'list']),
        side_nav_active: state.getIn(['ui', 'side_nav', 'active']), 
    };
}

export const CharacterContainer = connect(
        mapStateToProps, 
        actionCreators
        )(Character);

