import React from 'react';
import DocumentTitle from 'react-document-title';

export default class NoMatch extends React.Component {

    render() {
        return (
            <DocumentTitle title='Area FC: 404 Not Found'>
            <div className="content-body">
                <h3 className="text-primary text-center">404 Not Found</h3>
            </div>
            </DocumentTitle>
            );

    }

}